from flask import Flask, request
from module.controller.locations import LocationsController
import urllib

app = Flask(__name__)
locationsController = LocationsController()

class Router:
    def __init__(self):
        app.run(host='0.0.0.0', port=80, debug=True, threaded=True)

    @app.route('/userlocations')
    def locations():
        return locationsController.getLocations()

    @app.route('/userlocations/<user>', methods = ['POST'])
    def postlocations(user):
        return locationsController.postLocation(urllib.unquote_plus(user), request.form)

    #@app.route('/users/')
    #def users(userId):
    #    return locationsController.getUsers()

    #@app.route('/user/<userid>')
    #def user(userId):
    #    return locationsController.getUser(userId)

    #@app.route('/update/<user>/<location>')
    #def updateUser(user, location):
    #    return locationsController.updateUser(user, location)

    #@app.route('/signin/<user>/<password>')
    #def signIn(user, password):
    #    return locationsController.signIn(user, password)
    
    #@app.route('/signoff/<user>/<password>')
    #def signOff(user, password):
    #    return locationsController.signOff(user, password)
