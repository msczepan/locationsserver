from module.model.locations import LocationsModel

class LocationsController:

    def __init__(self):
        self.locationsModel = LocationsModel()

    def getLocations(self):
        return self.locationsModel.getLocations()
    
    def postLocation(self, user, data):
        return self.locationsModel.postLocation(user, data['lat'], data['lng'])
    #def getUsers(self):
    #    return self.locationsModel.getUsers()
    
    #def getUser(self, userId):
    #    return self.locationsModel.getUser(userId)
    
    #def updateUser(self, code, value):
    #    return self.locationsModel.updateBrand(code, value)
    
    #def signIn(self, user, password):
    #    return self.locationsModel.signIn(user, password)
    
    #def signOff(self, user, password):
    #    return self.locationsModel.signOff(user, password)
