from sqlobject import *
import MySQLdb
import sys
import json
from sqlobject.mysql import builder
from sqlobject.converters import sqlrepr
from sqlobject.sqlbuilder import *
from sqlobject.sqlbuilder import Alias

class LocationsModel:
    
    def __init__(self):
        self.conn = builder()(user='locache',  password='nICIRa', host='localhost', db='locache', port=3306)
        
    def getLocations(self):
        try:
            usersAlias = Alias('user_locations', "u")
            usersSelect = Select(
                ['username', 'lattitude', 'longitude'], 
                staticTables=[usersAlias],
                where='date_sub(NOW(),INTERVAL 10 MINUTE) < date_updated'
		#join = [LEFTJOIN(usersAlias, 'locations l ON u.id = l.user_id')]
            )
            query = self.conn.sqlrepr(usersSelect) 
	    print query
            rows = self.conn.queryAll(query)
            return json.dumps(rows, encoding = "ISO-8859-1")
        except MySQLdb.Error, e:
            print "Error %d: %s" % (e.args[0], e.args[1])

    def postLocation(self, user, lat, lng):
        query = "INSERT INTO user_locations (username, lattitude, longitude, date_created, date_updated) VALUES ('" + user + "', '" + lat + "', '" + lng + "', NOW(), NOW()) ON DUPLICATE KEY UPDATE lattitude = '" + lat + "', longitude = '" + lng + "', date_updated = NOW()";
        return self.execQuery(query, False);

    def getBrand(self, code):
        query = "SELECT code, description FROM zebra_erp_mappings_brands where code = '" + code + "'"
        return json.dumps(self.execQuery(query))

    def getUsers(self):
        try:
            usersAlias = Alias('users', "u")
            usersSelect = Select(
                ['u.id', 'name', 'surname', 'l.lattitude', 'l.longitude'], 
                staticTables=[usersAlias],
                join = [LEFTJOIN(usersAlias, 'locations l ON u.id = l.user_id')]
            )
            query = self.conn.sqlrepr(usersSelect) 
            rows = self.conn.queryAll(query)
            return json.dumps(rows, encoding = "ISO-8859-1")
        except MySQLdb.Error, e:
            print "Error %d: %s" % (e.args[0], e.args[1])

    def getUser(self, userId):
        try:
            usersAlias = Alias('users', "u")
            usersSelect = Select(
                ['u.id', 'name', 'surname', 'l.lattitude', 'l.longitude'], 
                staticTables=[usersAlias],
                join = [LEFTJOIN(usersAlias, 'locations l ON u.id = l.user_id')],
                where= ['u.id = ' . MySQLdb.quote(userId)]
            )
            query = self.conn.sqlrepr(usersSelect)
            print query 
            rows = self.conn.queryAll(query)
            return json.dumps(rows, encoding = "ISO-8859-1")
        except MySQLdb.Error, e:
            print "Error %d: %s" % (e.args[0], e.args[1])
    
    def updateUser(self, userId, location):
        return self.execQuery(query, False);
    
    def signIn(self, user, password):
        return self.execQuery(query, False);
    
    def signOff(self, code, value):
        if not code:
            return 'code is missing'
        query = "INSERT INTO zebra_erp_mappings_brands (code, description, updated_at) VALUES('" + code + "', '" + value + "', NOW())"
        return self.execQuery(query, False);
    
    def execQuery(self, query, readOnly = True):
        con = ''
        try:
            con = MySQLdb.connect(host='localhost', user='dstroynov', passwd='7a7iTEgiw47A', db='locache')

            with con:
                cur = con.cursor()##MySQLdb.cursors.DictCursor##)
                result = cur.execute(query)
                if readOnly:
                    return cur.fetchall()
            return json.dumps(result)
        except MySQLdb.Error, e:

            print "Error %d: %s" % (e.args[0], e.args[1])
            sys.exit(1)

        finally:
            if con:
                con.close()
